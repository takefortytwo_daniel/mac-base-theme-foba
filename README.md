# TAKEFORTYTWO README
# VERSION

{{cdn 'img/loading.svg'}}


# Base layout customizations
* Use {{#block 'html_id'}} {{/block}} on any layout in order to inject a specific layout ID into the base layout
* Use {{#block 'html_class'}} {{/block}} on any layout in order to inject a specific layout class into the base layout
* Use {{#block 'page_addition'}} {{/block}} on any layout in order to inject a specific layout script or style tag

# Theme options
* Upload Logo in Backend: Storefront > Logo
* Upload Favicon in Backend: Storefront > Logo
* Update header svg logo in templates > components common > store-logo
* Update footer svg logo in templates > components common > store-logo
* Update fonts in JSON in config.json: body-font and headings-font rows. If fonts are external, load them via assets/custom/fonts.
* Style default button in assets > scss > custom > theme.scss  

# Config
* Initial config.json theme settings are belong to us

#Consideration

To develop on the local stencil environment, make sure graphQl is a dependency on the package.json
    "graphql": "^14.4.2",
    "graphql-tag": "^2.10.1",
If you rather using Apollo to make the GraphQL front API calls refer to cornerstone apollo commit and config:

https://github.com/bigcommerce/cornerstone/commit/508feeb1b00d2bb2940771e5e91250a08b6be4d9#diff-7ae45ad102eab3b6d7e7896acd08c427a9b25b346470d7bc6507b6481575d519    

# QA
* Check error box in product. Line 9 of product-view
* Check all possible options and conditionals in test product (Ana)
* In order to test Credit Card transactions, cancellations and returns we need to make sure that products in the order/cart are not marked as invisible on the CP's product page.
 Because if that's the case, we won't be able to access any of the product's data from the frontend.
# Custom components
We have multiple html components under templates > components > custom. Each of them is already linked to a major native template on the theme, and contains a comment a the beginning explaining its purpose. You can add more custom templates anytime as long as you add proper comment and explanation.

We suggest using the "addition to head" or "addition to footer" blocks to inject any third party javascript (including jQuery) instead of using the BigCommerce's script manager.
The script manager cannot be synced at the theme level and is buggy.

# Script changes
* Small tweaks in product-details.js. Since this file is often updated in Cornerstone releases, we mark with a comment ("//T42") the beginning and the end of every change we make. Currently changes include disabling native gallery script and adding lines to hide or show back in stock notification form on variant change.


#Cornerstone updating known issues (from from 4.x to 5.1.x)

On the cornerstone update we have to make sure that the injects settings in the /templates/layout/base match in case and in variables called with the ones in the /assets/js/theme/common/product-details.js (in the new cornerstone are camelCased and in the old one was snake_cased).

We need to make sure that the parameters we call on the .js files from the injected js vars are in fact injected.

On the layout/base of the base theme productSize and zoomSize were missing, creating all sorts of js errors,

Also, in the new cornerstone all of the context.theme variables were moved to context.variableName in camel case.

#Custom fields for external specs pdf link
We added a handlebars if into the components/products/product-view.html template between lines 188-192 which allow us to style the 
content of that custom field as a button to an external or internal url in which the pdf with the full specifications
reside. The aforementioned field must the named fullSpecsButton in the product, otherwise, the template
will ignore it.

#Custom Search templates to use with the Stencil
There is two custom search "raw" templates in the structure, both are designed to return a JSon object with the
search result queried through window.stencilUtils.api.product.getById Stencil util, although with different purposes.
One is the /components/search/raw.html which is used for the custom variants (E.G. Benro "Amazon family" custom variants buttons),
which returns a list of products with the data to create the custom buttons.
And the other /components/search/raw-returns.html which is used in the return process to get the SKU(needed to keep
stock management through AlphaLAN) of the items based by querying the product id.

#X-Auth-Token for AlphaLAN
In order to mask the token and prevent it from being public on the scripts, we need to inject it on the jsContext.
To do that we'll add the following line to the layout/base.html template around line 45, with the rest of the injects.
{{~inject 'ALxToken' 'F757C2BA1C00212DCEEC00CC26CC8053B218C650EDDE4CE790542E81451E3F37'}}
Doing that allows us to have the token securerly accessible from any page.

#Product returns process
The return process enhances BigCommerce's native return process adding the capability to push the information directly
to AlphaLAN though a push of information to an API endpoint (https://aws.macgroupus.com/returnBigCommerceOrder).
Make sure the token is being injected into the jsContext on the templates/layout/base.html.

In order to activate returns we need to go to Advanced Settings/returns and enable the returns.

The script is located in the /pages/account/add-return.html template and retrieves the needed information
from the order via jQuery from a previous Handlebars "inject" used in the template for its ability to add information to the jsContext, which can be easily accessed through both jQuery.
The inject is around line 7 on the add-return.html:

{{inject "order_products" forms.return.order_products}}
{{inject "customer" customer}}
{{inject 'storeHash' settings.store_hash}}


and JavaScript and the product information by calling the window.stencilUtils.api.product.getById on each product of the order.
The return items custom script matches BigCommerce's ability to select how many of which items need to be returned.

The JSON object structure should be as follows:
{
    
    "storeId": "store hash id from BigCommerce(string)",
    "orderNumber": BigCommerce's order id(int),
    "customerName": "Customer full name(string)",
    "email": "customer@email(string)",
    "items": [
        {
            "qty": item quantity(int),
            "sku": "product-sku(string)"
        }
    ]
}


The script content is as follows:
<script>

    $(document).ready(function(){

        var jsContext = JSON.parse({{jsContext}});
        var storeId = jsContext.storeHash;
        var products = jsContext.order_products;
        var xToken  = jsContext.ALxToken;
        var orderNumber = "{{forms.return.order_id}}";
        var jsonObj = {};
        var items = [];

        $(".form-fieldset").on('change', (event) => {
            items = [];
            var j1 = $.map( products, function(product) {
                //get the qty from the selector
                var item = {};
                var qty = parseInt($('select[name="return_qty['+product.id+']"' ).children("option:selected"). val());
                if(qty > 0){
                    item['qty'] = qty;
                    window.stencilUtils.api.product.getById(product.product_id, { template: 'search/raw-returns' }, (err, resp) => {
                        var result = JSON.parse(jQuery.trim(resp));
                        item['sku'] = result;
                        items.push(item);
                    });
                }

            });
        });


        $('#return_form').on('submit', (event) => {

            event.preventDefault();
            jsonObj['storeId'] = storeId;
            jsonObj['orderNumber'] = orderNumber;
            jsonObj['customerName'] = jsContext.customer.name;
            jsonObj['email'] = jsContext.customer.email;
            jsonObj['items'] = items;
            var objTosend = JSON.stringify(jsonObj,null,4);

            //console.log(objTosend);

            if(items.length > 0){
                $.ajax({
                    type: "POST",
                    url: "https://aws.macgroupus.com/returnBigCommerceOrder",
                    // url: "https://servisuite.ngrok.io/returnBigCommerceOrder",
                    data: objTosend,
                    contentType: "text/plain",
                    headers: {
                        'X-Auth-Token' : xToken,
                        'Access-Control-Allow-Origin' : '*'
                    },
                    dataType: "json",
                    success: function() {
                        console.log(objTosend);
                        console.log("data sent");
                        $("#return_form").unbind('submit').submit();

                    },
                    error: function() {
                        $("#returnButton").prop("disabled",true);
                        $(".form-actions").prepend("<p id='return-error--text'>An Error Occurred, please refresh and try again, if it persists <a href='mailto:sales@shimodadesigns.com'>please email us.</a></p>");
                    }
                });
            }else{
               //console.log(items);

            }

        })



    });

</script>

#Order Cancellation process

The order cancellation process' goal is to allow the customer to cancel their order within a pre defined
window of time. Whether it be time framed or status dependent. Right now, the process is status depending
and it'll only show the cancellation link on the /components/account/order-list.html link to the /pages/account/order/details.html and the "Cancel Order" button under the actions on the pages/account/order/details.html template if the order status is "Awaiting fulfillment" or "Awaiting shipping". In both of these cases, the aforementioned link and button will appear respectively on the abovementioned templates.

In order to make it work we need to do the following:

Add option with the link below to the returns in the templates/components/account/order-list.html which in term, directs to the order's details.

Line 27(approx) Right below the returns if statement

{{#if this.status '==' 'Awaiting Fulfillment'}}
<a class="account-orderStatus-action cancel-order" id="{{this.id}}" href="{{this.details_url}}">
    Cancel Order
</a>
{{else}}
{{#if this.status '==' 'Awaiting Shipment'}}
<a class="account-orderStatus-action cancel-order" id="{{this.id}}" href="{{this.details_url}}">
    Cancel Order
</a>
{{/if}}
{{/if}}

Then, in the pages/account/order/details.html template, under the section actions, there's a new button which actions the script that triggers the order cancellation.

Inject on line 7

{{inject "order_products" order.items}}
{{inject "customer" customer}}
{{inject 'storeHash' settings.store_hash}}

Line 146 Right below the returns if statement

{{#if order.status_text '==' 'Awaiting Fulfillment'}}
<a href="" id="cancelOrder" class="button">Cancel Order </a>
{{else}}
{{#if order.status_text '==' 'Awaiting Shipment'}}
<a href="" id="cancelOrder" class="button">Cancel Order </a>
{{/if}}
{{/if}}

And at the bottom of the file inside script tags:
<script>
$(document).ready(function(){

    var jsContext = JSON.parse({{jsContext}});
    var storeId = jsContext.storeHash;
    var products = jsContext.order_products;
    var xToken  = jsContext.ALxToken;
    var orderNumber = "{{order.order_id}}";
    var jsonObj = {};




    $('#cancelOrder').on('click', (event) => {
        event.preventDefault();
        var items = [];
        jsonObj['transactionType'] = "C";
        jsonObj['storeId'] = storeId;
        jsonObj['orderNumber'] = {{order.id}};
        jsonObj['customerName'] = jsContext.customer.name;
        jsonObj['email'] = jsContext.customer.email;


        console.log(products);
        $.map(products, function(product){
            var item = {};
            item['qty'] = product.quantity;
            item['sku'] = product.sku;
            items.push(item);

        });
        jsonObj['items'] = items;
        var objTosend = JSON.stringify(jsonObj,null,4);
        console.log(objTosend);


        $.ajax({
            type: "POST",
            url: "https://aws.macgroupus.com/returnBigCommerceOrder",
            //url: "https://servisuite.ngrok.io/returnBigCommerceOrder",
            data: objTosend,
            contentType: "text/plain",
            headers: {
                'X-Auth-Token' : xToken,
                'Access-Control-Allow-Origin' : '*'
            },
            dataType: "json",
            success: function() {
                    $('.order-details-info').append("<div><p>The return has been processed, it might take up to 2 hours to refresh and you'll receive an email, plase don't submit the cancelation again.</p></div>");
                    console.log(objTosend);
                    console.log("data sent");
                    //$("#return_form").unbind('submit').submit();

                }, 
            error: function() {
                $("#cancelOrder").prop("disabled",true);
                // $(".form-actions").prepend("<p id='return-error--text'>An Error Occurred, please refresh and try again, if it persists <a href='mailto:sales@shimodadesigns.com'>please email us.</a></p>");
            }
        });


    })



});

</script>


The cancellation script per se, resides on the /pages/account/order-details.html and retrieves the needed information
from the order via jQuery from a previous Handlebars "inject" used in the template for its ability to add information to the jsContext, which can be easily accessed through both jQuery
and JavaScript and the product information by calling the window.stencilUtils.api.product.getById on each product of the order.
The information is then structures as follows to be send as an JSON object to an AlphaLAN API endpoint (https://aws.macgroupus.com/returnBigCommerceOrder):



The Json object structure should be as follows:
{
    "transactionType": "C",
    "storeId": "store hash id from BigCommerce(string)",
    "orderNumber": BigCommerce's order id(int),
    "customerName": "Customer full name(string)",
    "email": "customer@email(string)",
    "items": [
        {
            "qty": item quantity(int),
            "sku": "product-sku(string)"
        }
    ]
}

#Product reviews 


#Change gallery main image on variant change on product page

Add unitegallery main image to the js $scope on the assets/js/theme/common/product-detail.js inside the getViewModel($scope) method(approx line 181) by adding the following property to the list:

$selectedImage: $('.ug-slide-wrapper.ug-slide2 .ug-item-wrapper img', $scope), 

Add the getImageUrl(image) method to the assets/js/theme/common/product-detail.js. This method gets the image.data object BigCommerce's manipulates to modify the variant attributes and builds the image src url adding the size paramenter includede in the context-themeSettings.product_size and returns the correct variant image url (the image added to the variant level of the product which may not be included in the base product image list).
This url will be used to manipulate the DOM and add the url of the selected variant to the slide 2(the default image shown by the unitegallery) of the gallery, changing the image to match the selected variant at any point.

getImageUrl(image){
        if (_.isPlainObject(image)) {
            //console.log(image.data);
            const mainImageUrl = utils.tools.imageSrcset.getSrcset(
                image.data,
                {'1x': this.context.themeSettings.product_size},
                /*
                    Should match fallback image size used for the main product image in
                    components/products/product-view.html

                    Note that this will only be used as a fallback image for browsers that do not support srcset

                    Also note that getSrcset returns a simple src string when exactly one size is provided
                */
            );
            return mainImageUrl;
            //console.log(mainImageUrl);
        }else{

        }
    }

Then we need to add the action to effectively change the content of the image src on product variant change.
We'll achieve that by adding to the updateProductAttributes(data) method (located in assets/js/theme/common/product-detail.js line 665 approx) the following:

        const viewModel = this.getViewModel(this.$scope);
        //this line gets the full scope of the product page to be able to manipulate it
        const optionSrcUrl = this.getImageUrl(data.image);
        //this line gets the selected option image url to add it the default gallery slide
        viewModel.$selectedImage.attr('src', optionSrcUrl );
        //this line is the one actually modifiying the DOM to add the selected option image url to the default image slide in the gallery

That way, we make sure no only that the "main" gallery image is changed on option change, but also on the first product page load, if the default image doesn't match the default selected image in the gallery, the default option image will be the one loaded as the gallery's "main" image. 
Due to the loading order on the page, the product default image might show for an instant before the image is changed to the selected variant one.
Also, since we're modifying the default image on variant change, if we scroll through the gallery and land on a different image than the main one and change the variant, it'll not have any effect on the image being shown at that moment.
The script only takes care of changing the "default" gallery image.








#eAccountable/Impact referral link configuration,

Add universal tracking tag for eAccountable on the Storefront>Script Manager>Script Contents box on BigCommerce's control panel:

<!-- 1-Universal Tracking Code Script Manager -->
<script type="text/javascript"> (function(a,b,c,d,e,f,g){e['ire_o']=c;e[c]=e[c]||function(){(e[c].a=e[c].a||[]).push(arguments)};f=d.createElement(b);g=d.getElementsByTagName(b)[0];f.async=1;f.src=a;g.parentNode.insertBefore(f,g);})('https://d.impactradius-event.com/A1785237-b1a3-4a60-8e8b-356d0002473e1.js','script','ire',document,window); ire('identify', {customerid: '%%ORDER_ID%%' /*INSERT CUSTOMER ID*/, customeremail: '%%ORDER_EMAIL%%' /*INSERT SHA1 HASHED CUSTOMER EMAIL*/}); </script>
Add custom script to the pages/order-confirmation.html template.
Added on line 12 after {{{head.scripts}}}
<script>
var MD5 = function(d){result = M(V(Y(X(d),8*d.length)));return result.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_};

fetch('/api/storefront/order/{{checkout.order.id}}', {credentials: 'include'})
.then(function(response) {
return response.json();
})
.then(function(myJson) {

var orderId = myJson.orderId;
var customerId = myJson.customerId;
var customerEmail = myJson.billingAddress.email;
var promoCode;
var cartItems = [];

var custStatus= "NEW";
if(myJson.customerId !== 0){
   var custStatus= "RETURNING";
}

if(myJson.coupons.length > 0){
promoCode = myJson.coupons[0].code;
}

if(myJson.lineItems.digitalItems.length > 0){
for (var i = 0; i < myJson.lineItems.digitalItems.length; i++) {
        cartItems.push({sku: myJson.lineItems.digitalItems[i].sku, category: myJson.lineItems.digitalItems[i].type, quantity: myJson.lineItems.digitalItems[i].quantity, subTotal: myJson.lineItems.digitalItems[i].salePrice});
      }
}
if(myJson.lineItems.physicalItems.length > 0){
for (var i = 0; i < myJson.lineItems.physicalItems.length; i++) {
        cartItems.push({sku: myJson.lineItems.physicalItems[i].sku, category: myJson.lineItems.physicalItems[i].type, quantity: myJson.lineItems.physicalItems[i].quantity, subTotal: myJson.lineItems.physicalItems[i].salePrice});
      }
}

var customerEmailhashed = MD5(customerEmail);

console.log(orderId);
console.log(customerId);
console.log(customerEmail);
console.log(customerEmailhashed);
console.log(custStatus);
console.log(promoCode);
console.log(cartItems);

ire('trackConversion', 18362, {
        orderId: orderId,
        customerId: customerId,
        customerEmail: customerEmailhashed,
        customerStatus: custStatus,
        orderPromoCode: promoCode,
        items: cartItems
    },{verifySiteDefinitionMatch:true});

});
</script>
We need to confirm if it's neccessary to modify the id of teh trackConversion for each store or if all of them are under the same account in impact.


# CSS structure
Styles should be split among multiple custom css stylesheets in order to keep the code as modular as possible.
Refer to scss/theme.scss line 88 to see the details.

# server_files folder
The contents on this folder should be copied to the /dav/content of your store's WebDAV directory.
The files include:
- Gallery: Unite Gallery code, used in all Product layouts. See https://unitegallery.net/index.php?page=compact-options for documentation.
- Menu: MMenu script, used for mobile menu. See https://mmenujs.com/ for documentation.

# Pending ideas
We'd like to be able to use the schema or package files to activate or deactivate certain things that will not be present on every website, including:
* Read more on product description, specs and overview
* Country select on top left
* Custom variant script loader
* Top bar + Free shipping
* Faceted search
* Newsletter pop-up
* Collections hard-coded tab
* Wishlists
* Compare
* QuickView
* Custom variant script loader
* Use availability for custom text next to sku in product page


# PENDING TASKS
* Ajustar envelope via iconify (back in stock button)










# CORNERSTONE BY BIGCOMMERCE README
# Cornerstone
[![Build Status](https://travis-ci.org/bigcommerce/cornerstone.svg?branch=master)](https://travis-ci.org/bigcommerce/cornerstone)

Stencil's Cornerstone theme is the building block for BigCommerce theme developers to get started quickly developing premium quality themes on the BigCommerce platform.

### Stencil Utils
[Stencil-utils](https://github.com/bigcommerce/stencil-utils) is our supporting library for our events and remote interactions.

## JS API
When writing theme JavaScript (JS) there is an API in place for running JS on a per page basis. To properly write JS for your theme, the following page types are available to you:

* "pages/account/addresses"
* "pages/account/add-address"
* "pages/account/add-return"
* "pages/account/add-wishlist"
* "pages/account/recent-items"
* "pages/account/download-item"
* "pages/account/edit"
* "pages/account/return-saved"
* "pages/account/returns"
* "pages/account/payment-methods"
* "pages/auth/login"
* "pages/auth/account-created"
* "pages/auth/create-account"
* "pages/auth/new-password"
* "pages/blog"
* "pages/blog-post"
* "pages/brand"
* "pages/brands"
* "pages/cart"
* "pages/category"
* "pages/compare"
* "pages/errors"
* "pages/gift-certificate/purchase"
* "pages/gift-certificate/balance"
* "pages/gift-certificate/redeem"
* "global"
* "pages/home"
* "pages/order-complete"
* "pages/page"
* "pages/product"
* "pages/search"
* "pages/sitemap"
* "pages/subscribed"
* "pages/account/wishlist-details"
* "pages/account/wishlists"

These page types will correspond to the pages within your theme. Each one of these page types map to an ES6 module that extends the base `PageManager` abstract class.

```javascript
    export default class Auth extends PageManager {
        constructor() {
            // Set up code goes here; attach to internals and use internals as you would 'this'
        }
    }
```

### JS Template Context Injection
Occasionally you may need to use dynamic data from the template context within your client-side theme application code.

Two helpers are provided to help achieve this.

The inject helper allows you to compose a JSON object with a subset of the template context to be sent to the browser.

```
{{inject "stringBasedKey" contextValue}}
```

To retrieve the parsable JSON object, just call `{{jsContext}}` after all of the `{{@inject}}` calls.

For example, to setup the product name in your client-side app, you can do the following if you're in the context of a product:

```html
{{inject "myProductName" product.title}}

<script>
// Note the lack of quotes around the jsContext handlebars helper, it becomes a string automatically.
var jsContext = JSON.parse({{jsContext}}); // jsContext would output "{\"myProductName\": \"Sample Product\"}" which can feed directly into your JavaScript

console.log(jsContext.myProductName); // Will output: Sample Product
</script>
```

You can compose your JSON object across multiple pages to create a different set of client-side data depending on the currently loaded template context.

The stencil theme makes the jsContext available on both the active page scoped and global PageManager objects as `this.context`.

## Polyfilling via Feature Detection
Cornerstone implements [this strategy](https://philipwalton.com/articles/loading-polyfills-only-when-needed/) for polyfilling.

In `templates/components/common/polyfill-script.html` there is a simple feature detection script which can be extended to detect any recent JS features you intend to use in your theme code.

If any one of the conditions is not met, an additional blocking JS bundle configured in `assets/js/polyfills.js` will be loaded to polyfill modern JS features before the main bundle executes. 

This intentionally prioritizes the experience of the 90%+ of shoppers who are on modern browsers in terms of performance, while maintaining compatibility (at the expense of additional JS download+parse for the polyfills) for users on legacy browsers.

## Static assets
Some static assets in the Stencil theme are handled with Grunt if required. This
means you have some dependencies on grunt and npm. To get started:

First make sure you have Grunt installed globally on your machine:

```
npm install -g grunt-cli
```

and run:

```
npm install
```

Note: package-lock.json file was generated by Node version 10 and npm version 6.11.3. The app supports Node 10 as well as multiple versions of npm, but we should always use those versions when updating package-lock.json, unless it is decided to upgrade those, and in this case the readme should be updated as well. If using a different version for node OR npm, please delete the package-lock.json file prior to installing node packages and also prior to pushing to github.

If updating or adding a dependency, please double check that you are working on Node version 10 and npm version 6.11.3 and run ```npm update <package_name>```  or ```npm install <package_name>``` (avoid running npm install for updating a package). After updating the package, please make sure that the changes in the package-lock.json reflect only the updated/new package prior to pushing the changes to github.


### Icons
Icons are delivered via a single SVG sprite, which is embedded on the page in
`templates/layout/base.html`. It is generated via a grunt task `grunt svgstore`.

The task takes individual SVG files for each icon in `assets/icons` and bundles
them together, to be inlined on the top of the theme, via an ajax call managed
by svg-injector. Each icon can then be called in a similar way to an inline image via:

```
<svg><use xlink:href="#icon-svgFileName" /></svg>
```

The ID of the SVG icon you are calling is based on the filename of the icon you want,
with `icon-` prepended. e.g. `xlink:href="#icon-facebook"`.

Simply add your new icon SVG file to the icons folder, and run `grunt svgstore`,
or just `grunt`.

#### License

(The MIT License)
Copyright (C) 2015-present BigCommerce Inc.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
